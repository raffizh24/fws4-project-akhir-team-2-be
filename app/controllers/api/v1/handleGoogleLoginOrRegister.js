const axios = require("axios");
const jwt = require("jsonwebtoken");
const {Users} = require("../../../models");
const JWT_SIGNATURE_KEY = process.env.JWT_SIGNATURE_KEY;

function createToken(user) {
    const payload = {
        id: user.id,
        name: user.name,
        email: user.email,
    };

    return jwt.sign(payload, JWT_SIGNATURE_KEY, {
        expiresIn: "1d", // 1 days
    });
}

async function handleGoogleLoginOrRegister(req, res) {
    const {access_token} = req.body;

    try {
        const response = await axios.get(`https://www.googleapis.com/oauth2/v3/userinfo?access_token=${access_token}`);
        const {sub, email, name} = response.data;

        let user = await Users.findOne({where: {googleId: sub}});
        if (!user)
            user = await Users.create({
                typeUser: 2,
                googleId: sub,
                email,
                name,
            });

        delete user.encryptedPassword;

        const token = createToken(user);

        res.status(201).json({token, user});
    } catch (err) {
        res.status(401).json({error: {name: err.name, message: err.message}});
    }
}

module.exports = handleGoogleLoginOrRegister;
