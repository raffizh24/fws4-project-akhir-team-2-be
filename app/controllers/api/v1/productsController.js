const productsService = require("../../../services/productsService");
const jwt = require("jsonwebtoken");

const {promisify} = require("util");
const cloudinary = require("../../../../config/cloudinary");
const cloudinaryUpload = promisify(cloudinary.uploader.upload);
const cloudinaryDestroy = promisify(cloudinary.uploader.destroy);

function verifyToken(token) {
    try {
        return jwt.verify(token, "Rahasia");
    } catch (error) {
        throw new Error("Token expired");
    }
}

module.exports = {
    listAllProducts: async (req, res) => {
        try {
            let tokenPayload = {id: null};
            if (req.headers.authorization !== "") {
                const bearerToken = req.headers.authorization;
                const token = bearerToken.split("Bearer ")[1];
                tokenPayload = await verifyToken(token);
            }

            productsService
                .list({id: tokenPayload.id})
                .then((data) => {
                    res.status(200).json(data);
                })
                .catch((err) => {
                    res.status(500).json({
                        error: err.message,
                    });
                });
        } catch (error) {
            res.status(500).json({
                error: error.message,
            });
        }
    },

    getProductsByStatus(req, res) {
        try {
            if (req.query.statusProduk === "All") {
                productsService
                    .findByIdSeller(req.query.idSeller)
                    .then((data) => {
                        res.status(200).json(data);
                    })
                    .catch((err) => {
                        res.status(500).json({
                            error: err.message,
                        });
                    });
            } else {
                productsService
                    .listByStatus(req.query.idSeller, req.query.statusProduk)
                    .then((data) => {
                        res.status(200).json(data);
                    })
                    .catch((err) => {
                        res.status(500).json({
                            error: err.message,
                        });
                    });
            }
        } catch (error) {
            res.status(500).json({
                error: error.message,
            });
        }
    },

    getProductById: async (req, res) => {
        try {
            const product = await productsService.getById(req.params.id);
            res.status(200).json(product);
        } catch (error) {
            res.status(500).json({
                error: error.message,
            });
        }
    },

    getProductByKategori: async (req, res) => {
        try {
            let tokenPayload = {id: null};
            if (req.headers.authorization !== "") {
                const bearerToken = req.headers.authorization;
                const token = bearerToken.split("Bearer ")[1];
                tokenPayload = await verifyToken(token);
            }

            const product = await productsService.getByKategori({id: tokenPayload.id, kategori: req.query.kategori});
            res.status(200).json(product);
        } catch (error) {
            res.status(500).json({
                error: error.message,
            });
        }
    },

    getProductByName: async (req, res) => {
        try {
            const namaProduk = req.query.namaProduk.toLowerCase();
            const product = await productsService.getByNama(namaProduk);
            res.status(200).json(product);
        } catch (error) {
            res.status(500).json({
                error: error.message,
            });
        }
    },

    createProduct: async (req, res) => {
        try {
            const bearerToken = req.headers.authorization;
            const token = bearerToken.split("Bearer ")[1];
            const tokenPayload = await verifyToken(token);
            const idSeller = tokenPayload.id;

            const {namaProduk, hargaProduk, kategori, deskripsi} = req.body;
            const fotoProduk = [];
            const fileBase64 = [];
            const file = [];

            for (var i = 0; i < req.files.length; i++) {
                fileBase64.push(req.files[i].buffer.toString("base64"));
                file.push(`data:${req.files[i].mimetype};base64,${fileBase64[i]}`);
                const result = await cloudinaryUpload(file[i]);
                fotoProduk.push(result.secure_url);
            }

            const createArgs = {idSeller, namaProduk, hargaProduk, kategori, deskripsi, fotoProduk, statusProduk: "Tersedia"};

            productsService.create(createArgs).then((product) => {
                res.status(201).json({
                    status: "PRODUCT_CREATED",
                    product,
                });
            });
        } catch (error) {
            res.status(500).json({
                error: error.message,
            });
        }
    },

    updateProduct: async (req, res) => {
        const bearerToken = req.headers.authorization;
        const token = bearerToken.split("Bearer ")[1];
        const tokenPayload = await verifyToken(token);
        const idSeller = tokenPayload.id;

        const {namaProduk, hargaProduk, kategori, deskripsi, statusProduk, oldImage} = req.body;
        const fotoProduk = [];
        const fileBase64 = [];
        const file = [];

        try {
            // Delete Image from Cloudinary
            if (oldImage !== undefined) {
                if (Array.isArray(oldImage)) {
                    // Kalo bentuknya array
                    for (var x = 0; x < oldImage.length; x++) {
                        cloudinaryDestroy(oldImage[x]);
                    }
                } else {
                    // Kalo bentuknya string cuma 1 image
                    cloudinaryDestroy(oldImage);
                }
            }
            // Already Image in Cloudinary

            if (req.body.fotoProduk !== undefined) {
                if (Array.isArray(req.body.fotoProduk)) {
                    for (var y = 0; y < req.body.fotoProduk.length; y++) {
                        fotoProduk[y] = req.body.fotoProduk[y];
                    }
                } else {
                    fotoProduk.push(req.body.fotoProduk);
                }
            }

            // Upload New Image to Cloudinary
            if (req.files.length > 0) {
                for (var i = 0; i < req.files.length; i++) {
                    fileBase64.push(req.files[i].buffer.toString("base64"));
                    file.push(`data:${req.files[i].mimetype};base64,${fileBase64[i]}`);
                    const result = await cloudinaryUpload(file[i]);
                    fotoProduk.push(result.secure_url);
                }
            }

            let updateArgs = {
                idSeller,
                namaProduk,
                hargaProduk,
                kategori,
                deskripsi,
                statusProduk,
                fotoProduk,
            };

            productsService.update(req.params.id, updateArgs).then(() => {
                res.status(200).json({
                    status: "OK",
                    message: "Product updated",
                });
            });
        } catch (error) {
            res.status(500).json({
                error: error.message,
            });
        }
    },

    deleteProduct: async (req, res) => {
        try {
            const bearerToken = req.headers.authorization;
            const token = bearerToken.split("Bearer ")[1];
            const tokenPayload = await verifyToken(token);
            const idSeller = tokenPayload.id;
            const {id, oldImage} = req.query;

            // Delete Image from Cloudinary
            if (oldImage !== undefined) {
                if (Array.isArray(oldImage)) {
                    // Kalo bentuknya array
                    for (var x = 0; x < oldImage.length; x++) {
                        cloudinaryDestroy(oldImage[x]);
                    }
                } else {
                    // Kalo bentuknya string cuma 1 image
                    cloudinaryDestroy(oldImage);
                }
            }

            productsService.delete(id, idSeller).then(() => {
                res.status(200).json({
                    status: "OK",
                    message: "Product deleted",
                });
            });
        } catch (error) {
            res.status(500).json({
                error: error.message,
            });
        }
    },
};
