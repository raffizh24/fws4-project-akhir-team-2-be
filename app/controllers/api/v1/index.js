const handleGoogleLoginOrRegister = require("./handleGoogleLoginOrRegister");
const productsController = require("./productsController");

module.exports = {
    handleGoogleLoginOrRegister,
    productsController,
};
