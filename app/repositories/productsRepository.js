const {Products, Users, Transactions} = require("../models");
const {Op} = require("sequelize");
module.exports = {
    findAll(Args) {
        return Products.findAll({
            where: {
                [Op.and]: [
                    {
                        idSeller: {
                            [Op.ne]: Args.id,
                        },
                    },
                    {statusProduk: {[Op.ne]: "Terjual"}},
                ],
            },
            include: Users,
        });
    },

    findById(id) {
        return Products.findByPk(id, {include: Users});
    },

    findByNama(namaProduk) {
        return Products.findAll({
            where: {
                namaProduk: {
                    [Op.iLike]: `%${namaProduk}%`,
                },
            },
        });
    },

    findByKategori(Args) {
        return Products.findAll({
            where: {
                [Op.and]: [
                    {
                        idSeller: {
                            [Op.ne]: Args.id,
                        },
                    },
                    {kategori: Args.kategori},
                    {statusProduk: {[Op.ne]: "Terjual"}},
                ],
            },
        });
    },

    findByStatus(idSeller, statusProduk) {
        return Products.findAll({
            where: {
                [Op.and]: [{idSeller}, {statusProduk}],
            },
        });
    },

    findByIdSeller(idSeller) {
        return Products.findAll({
            where: {
                idSeller,
            },
        });
    },

    create(createArgs) {
        return Products.create(createArgs);
    },

    update(id, updateArgs) {
        return Products.update(updateArgs, {
            where: {
                id,
                idSeller: updateArgs.idSeller,
            },
        });
    },

    async delete(id, idSeller) {
        await Transactions.destroy({where: {idProduk: id}});
        return Products.destroy({where: {id, idSeller}});
    },
};
