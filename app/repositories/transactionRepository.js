const {Transactions, Products, Users} = require("../models");
const {Op} = require("sequelize");

module.exports = {
    getByIdBuyer(getArgs) {
        return Transactions.findAll({
            include: [
                {
                    model: Products,
                },
                {
                    model: Users,
                },
            ],
            where: {
                idBuyer: getArgs,
            },
        });
    },

    getByIdSeller(getArgs) {
        return Transactions.findAll({
            include: [
                {
                    model: Products,
                    where: {idSeller: getArgs},
                },
                {
                    model: Users,
                },
            ],
            where: {
                status: {
                    [Op.or]: ["Menunggu", "Diproses"],
                },
            },
        });
    },

    create(createArgs) {
        Products.update({statusProduk: "Diminati"}, {where: {id: createArgs.idProduk}});
        return Transactions.create(createArgs);
    },

    async update(id, status) {
        return Transactions.update({status}, {where: {id}});
    },

    async updateStatus(args) {
        const {id, idProduk, status, statusProduk} = args;
        await Products.update({statusProduk: statusProduk}, {where: {id: idProduk}});
        await Transactions.update({status}, {where: {id}});
    },

    updateAllTransaction(idProduk) {
        Transactions.update({status: "Ditolak"}, {where: {idProduk}});
    },

    delete(id) {
        return Transactions.destroy({where: {id}});
    },
};
