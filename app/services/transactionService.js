/* eslint-disable no-useless-catch */
const transactionRepository = require("../repositories/transactionRepository");

module.exports = {
    getByIdBuyer(getArgs) {
        return transactionRepository.getByIdBuyer(getArgs);
    },

    getByIdSeller(getArgs) {
        return transactionRepository.getByIdSeller(getArgs);
    },

    create(createArgs) {
        return transactionRepository.create(createArgs);
    },

    update(id, status) {
        return transactionRepository.update(id, status);
    },

    updateStatus(args) {
        return transactionRepository.updateStatus(args);
    },

    updateAllTransaction(args) {
        return transactionRepository.updateAllTransaction(args);
    },

    delete(id) {
        return transactionRepository.delete(id);
    },
};
