/* eslint-disable no-useless-catch */
const productsRepository = require("../repositories/productsRepository");

module.exports = {
    create(requestBody) {
        return productsRepository.create(requestBody);
    },

    update(id, requestBody) {
        return productsRepository.update(id, requestBody);
    },

    delete(id, idSeller) {
        return productsRepository.delete(id, idSeller);
    },

    getById(id) {
        return productsRepository.findById(id);
    },

    getByKategori(Args) {
        return productsRepository.findByKategori(Args);
    },

    getByNama(namaProduk) {
        return productsRepository.findByNama(namaProduk);
    },

    async list(Args) {
        return productsRepository.findAll(Args);
    },

    async listByStatus(idSeller, statusProduk) {
        return await productsRepository.findByStatus(idSeller, statusProduk);
    },

    async findByIdSeller(idSeller) {
        return await productsRepository.findByIdSeller(idSeller);
    },
};
