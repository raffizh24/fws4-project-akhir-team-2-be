"use strict";
const {Model} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Users extends Model {
        static associate(models) {
            models.Users.belongsTo(models.Type_Users, {
                foreignKey: "typeUser",
            });
            models.Users.hasMany(models.Transactions, {
                foreignKey: "idBuyer",
            });
            models.Users.hasMany(models.Products, {
                foreignKey: "idSeller",
            });
        }
    }
    Users.init(
        {
            typeUser: DataTypes.INTEGER,
            googleId: DataTypes.STRING,
            email: DataTypes.STRING,
            password: DataTypes.STRING,
            nama: DataTypes.STRING,
            kota: DataTypes.STRING,
            alamat: DataTypes.STRING,
            noHp: DataTypes.STRING,
            fotoUser: DataTypes.STRING,
        },
        {
            sequelize,
            modelName: "Users",
        }
    );
    return Users;
};
