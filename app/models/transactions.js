"use strict";
const {Model} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Transactions extends Model {
        static associate(models) {
            models.Transactions.belongsTo(models.Products, {
                foreignKey: "idProduk",
            });
            models.Transactions.belongsTo(models.Users, {
                foreignKey: "idBuyer",
            });
        }
    }
    Transactions.init(
        {
            idBuyer: DataTypes.INTEGER,
            idProduk: DataTypes.INTEGER,
            penawaran: DataTypes.INTEGER,
            status: DataTypes.STRING,
        },
        {
            sequelize,
            modelName: "Transactions",
        }
    );
    return Transactions;
};
