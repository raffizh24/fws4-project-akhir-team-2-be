"use strict";
const {Model} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Products extends Model {
        static associate(models) {
            models.Products.belongsTo(models.Users, {
                foreignKey: "idSeller",
            });
            models.Products.hasMany(models.Transactions, {
                foreignKey: "idProduk",
            });
            models.Products.hasOne(models.Users, {
                foreignKey: "id",
            });
            models.Products.belongsTo(models.Users, {
                foreignKey: "idSeller",
            });
        }
    }
    Products.init(
        {
            idSeller: DataTypes.INTEGER,
            namaProduk: DataTypes.STRING,
            hargaProduk: DataTypes.INTEGER,
            kategori: DataTypes.STRING,
            deskripsi: DataTypes.STRING,
            fotoProduk: {
                type: DataTypes.ARRAY(DataTypes.STRING),
                // get() {
                //     const rawValue = this.getDataValue("fotoProduk");
                //     return rawValue ? "http://localhost:8000/" + rawValue.substring(1) : null;
                // },
            },
            statusProduk: DataTypes.STRING,
        },
        {
            sequelize,
            modelName: "Products",
        }
    );
    return Products;
};
