"use strict";
const {Model} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Type_Users extends Model {
        static associate(models) {
            models.Type_Users.hasMany(models.Users, {
                foreignKey: "typeUser",
            });
        }
    }
    Type_Users.init(
        {
            typeUser: DataTypes.STRING,
        },
        {
            sequelize,
            modelName: "Type_Users",
        }
    );
    return Type_Users;
};
