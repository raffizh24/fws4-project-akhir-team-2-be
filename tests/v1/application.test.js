const request = require("supertest");
const app = require("../../app");

describe("Application Test", () => {
    it("500 Internal Server Error", async () => {
        await request(app)
            .get("/api/v1/errors")
            .then((res) => {
                expect(res.statusCode).toBe(500);
            });
    });

    it("404 Not Found", async () => {
        await request(app)
            .get("/invalid")
            .then((res) => {
                expect(res.statusCode).toBe(404);
            });
    });
});
