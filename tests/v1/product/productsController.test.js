const request = require("supertest");
const app = require("../../../app");
const argsLogin = {email: "jest@gmail.com", password: "123"}; // Login
const tokenExp =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidHlwZVVzZXIiOjIsImVtYWlsIjoicG9zdG1hbkBnbWFpbC5jb20iLCJpYXQiOjE2NTY5MzYwNzMsImV4cCI6MTY1NjkzOTY3M30.f5JtKkdVgY_5Z6J3m1BJzMqthCghD12wo1dC6p_IWHM";
let token200 = "";
let productId = "";
let userId = "";

beforeAll(async () => {
    return await request(app)
        .post("/api/v1/login")
        .send(argsLogin)
        .then((res) => {
            token200 = res.body.token;
            userId = res.body.user.id;
        });
});

afterAll(async () => {
    return await request(app).delete("/api/v1/product").set("Authorization", `Bearer ${token200}`).query({id: productId});
});

describe("API Product", () => {
    it("Get All Product Jika sudah Login -> response code 200", async () => {
        return await request(app)
            .get("/api/v1/products")
            .set("Authorization", `Bearer ${token200}`)
            .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Get All Product Jika belum Login -> response code 200", async () => {
        return await request(app)
            .get("/api/v1/products")
            .set("Authorization", "")
            .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Get Product by ID -> response code 200", async () => {
        return await request(app)
            .get("/api/v1/product/1")
            .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Get Product by ID (params bukan angka) -> response code 500", async () => {
        return await request(app)
            .get("/api/v1/product/invalid")
            .then((res) => {
                expect(res.statusCode).toBe(500);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Filter by Kategori Produk -> response code 200", async () => {
        return await request(app)
            .get("/api/v1/product/filter/kategori")
            .set("Authorization", `Bearer ${token200}`)
            .query({kategori: "Hobi"})
            .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Filter by Kategori Produk -> response code 500", async () => {
        return await request(app)
            .get("/api/v1/product/filter/kategori")
            .set("Authorization", `Bearer ${token200}`)
            .then((res) => {
                expect(res.statusCode).toBe(500);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Filter by Status Produk -> response code 200", async () => {
        return await request(app)
            .get("/api/v1/product/filter/status")
            .query({idSeller: userId, statusProduk: "All"})
            .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Filter by Status Produk -> response code 200", async () => {
        return await request(app)
            .get("/api/v1/product/filter/status")
            .query({idSeller: userId, statusProduk: "Diminati"})
            .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Filter by Nama Produk -> response code 200", async () => {
        return await request(app)
            .get("/api/v1/product/filter/nama")
            .query({namaProduk: "jam"})
            .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Filter by Nama Produk (nama produk kosong) -> response code 500", async () => {
        return await request(app)
            .get("/api/v1/product/filter/nama")
            .query({namaProduks: ""})
            .then((res) => {
                expect(res.statusCode).toBe(500);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Create Product -> response code 201", async () => {
        return await request(app)
            .post("/api/v1/product")
            .set("Authorization", `Bearer ${token200}`)
            .attach("fotoProduk", "")
            .field({
                namaProduk: "Jest",
                hargaProduk: "100000",
                kategoriProduk: "Hobi",
                deskripsiProduk: "Jam",
                statusProduk: "Tersedia",
                idSeller: userId,
            })
            .then((res) => {
                expect(res.statusCode).toBe(201);
                expect(res.body).toEqual(expect.any(Object));
                productId = res.body.product.id;
            });
    });

    it("Create Product (Tidak ada formData) -> response code 500", async () => {
        return await request(app)
            .post("/api/v1/product")
            .set("Authorization", `Bearer ${token200}`)
            .then((res) => {
                expect(res.statusCode).toBe(500);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Edit Product (form data kosong) -> response code 500", async () => {
        return await request(app)
            .put("/api/v1/product/1")
            .set("Authorization", `Bearer ${token200}`)
            .then((res) => {
                expect(res.statusCode).toBe(500);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Delete Product (Old Image String) -> response code 200", async () => {
        return await request(app)
            .delete("/api/v1/product")
            .set("Authorization", `Bearer ${token200}`)
            .query({id: productId, oldImage: "invalid"})
            .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Delete Product (Old Image Array) -> response code 200", async () => {
        return await request(app)
            .delete("/api/v1/product")
            .set("Authorization", `Bearer ${token200}`)
            .query({id: productId, oldImage: ["invalid", "invalid"]})
            .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("Token Expired -> response code 500", async () => {
        return await request(app)
            .get("/api/v1/products")
            .set("Authorization", `Bearer ${tokenExp}`)
            .then((res) => {
                expect(res.statusCode).toBe(500);
                expect(res.body).toEqual(expect.any(Object));
            });
    });
});
