const request = require("supertest");
const app = require("../../../app");

const args400 = {emails: "400@gmail.com", passwords: "123", names: "Jest"}; // Salah nama field
const argsLogin = {email: "jest@gmail.com", password: "123"}; // Login
const argsRegister = {email: "jestregis@gmail.com", password: "123", name: "Jest"}; // Register
const login401 = {email: "jest@gmail.com", password: "wrong"}; // Wrong Password
const login404 = {email: "invalid404@gmail.com", password: "123"}; // Invalid Email
const user = {nama: "JEST", kota: "SYSTEM", alamat: "SYSTEM", noHp: "+628123456789"}; // Data User Update
const user400 = {nama: {nama: "JEST", nama1: "Jest"}}; // Salah nama field
const token401 =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidHlwZVVzZXIiOjIsImVtYWlsIjoicG9zdG1hbkBnbWFpbC5jb20iLCJpYXQiOjE2NTY5MzYwNzMsImV4cCI6MTY1NjkzOTY3M30.f5JtKkdVgY_5Z6J3m1BJzMqthCghD12wo1dC6p_IWHM"; // Unauthorized
let token200 = ""; // Token Login

beforeAll(async () => {
    // Get Token
    return await request(app)
        .post("/api/v1/login")
        .send(argsLogin)
        .then((res) => {
            token200 = res.body.token;
        });
});

afterAll(async () => {
    return await request(app)
        .delete(`/api/v1/auth/delete/${argsRegister.email}`)
        .then((res) => {
            expect(res.statusCode).toBe(200);
            expect(res.body).toEqual({
                status: "DELETE_SUCCESS",
            });
        });
});

describe("POST /api/v1/register", () => {
    it("REGISTER - Jika Register berhasil -> response code 201", async () => {
        return await request(app)
            .post("/api/v1/register")
            .send(argsRegister)
            .then((res) => {
                expect(res.statusCode).toBe(201);
                expect(res.body).toEqual({
                    status: expect.any(String),
                    user: expect.any(Object),
                });
            });
    });

    it("REGISTER - Jika Email sudah terdaftar -> response code 409", async () => {
        return await request(app)
            .post("/api/v1/register")
            .send(argsLogin)
            .then((res) => {
                expect(res.statusCode).toBe(409);
                expect(res.body).toEqual({
                    status: "FAIL",
                    message: "Email telah digunakan",
                });
            });
    });

    it("REGISTER - Jika nama field salah -> response code 400", async () => {
        return await request(app)
            .post("/api/v1/register")
            .send(args400)
            .then((res) => {
                expect(res.statusCode).toBe(400);
            });
    });

    it("LOGIN - Jika Login berhasil -> response code 200", async () => {
        return await request(app)
            .post("/api/v1/login")
            .send(argsLogin)
            .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual({
                    token: expect.any(String),
                    user: expect.any(Object),
                });
            });
    });

    it("LOGIN - Jika Email belum terdaftar -> response code 404", async () => {
        return await request(app)
            .post("/api/v1/login")
            .send(login404)
            .then((res) => {
                expect(res.statusCode).toBe(404);
                expect(res.body).toEqual({
                    message: "Email salah",
                });
            });
    });

    it("LOGIN - Jika Password salah -> response code 401", async () => {
        return await request(app)
            .post("/api/v1/login")
            .send(login401)
            .then((res) => {
                expect(res.statusCode).toBe(401);
                expect(res.body).toEqual({
                    message: "Password salah",
                });
            });
    });

    it("LOGIN - Jika nama field salah -> response code 400", async () => {
        return await request(app)
            .post("/api/v1/login")
            .send(args400)
            .then((res) => {
                expect(res.statusCode).toBe(400);
            });
    });

    it("WHOAMI - Jika Token valid -> response code 200", async () => {
        return await request(app)
            .get("/api/v1/auth/me")
            .set("Authorization", `Bearer ${token200}`)
            .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual({
                    user: expect.any(Object),
                });
            });
    });

    it("WHOAMI - Jika Token Expired -> response code 401", async () => {
        return await request(app)
            .get("/api/v1/auth/me")
            .set("Authorization", `Bearer ${token401}`)
            .then((res) => {
                expect(res.statusCode).toBe(401);
                expect(res.body).toEqual({
                    status: "FAILED",
                    message: "Token expired",
                });
            });
    });

    it("UPDATE INFO USER - Jika Token valid -> response code 200", async () => {
        return await request(app)
            .put("/api/v1/auth/me")
            .set("Authorization", `Bearer ${token200}`)
            .attach("fotoUser", "")
            .field(user)
            .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual({
                    status: "UPDATE_SUCCESS",
                    data: expect.any(Object),
                });
            });
    });

    it("UPDATE INFO USER - Jika gagal update info user -> response code 400", async () => {
        return await request(app)
            .put("/api/v1/auth/me")
            .set("Authorization", `Bearer ${token200}`)
            .send(user400)
            .then((res) => {
                expect(res.statusCode).toBe(400);
                expect(res.body).toEqual(expect.any(Object));
            });
    });

    it("UPDATE INFO USER - Jika Token Expired -> response code 401", async () => {
        return await request(app)
            .put("/api/v1/auth/me")
            .set("Authorization", `Bearer ${token401}`)
            .then((res) => {
                expect(res.statusCode).toBe(401);
                expect(res.body).toEqual(expect.any(Object));
            });
    });
});
