/* eslint-disable no-unused-vars */
"use strict";
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable("Products", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            idSeller: {
                type: Sequelize.INTEGER,
                references: {
                    model: "Users",
                    key: "id",
                },
            },
            namaProduk: {
                type: Sequelize.STRING,
            },
            hargaProduk: {
                type: Sequelize.INTEGER,
            },
            kategori: {
                type: Sequelize.STRING,
            },
            deskripsi: {
                type: Sequelize.STRING,
            },
            fotoProduk: {
                type: Sequelize.ARRAY(Sequelize.STRING),
            },
            statusProduk: {
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable("Products");
    },
};
