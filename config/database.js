require("dotenv").config();
const {DB_USER, DB_PASSWORD, DB_NAME, DB_HOST, DB_PORT} = process.env;

module.exports = {
    // Heroku Postgres
    development: {
        username: DB_USER,
        password: DB_PASSWORD,
        database: DB_NAME,
        host: DB_HOST,
        port: DB_PORT,
        dialect: "postgres",
        ssl: true,
        dialectOptions: {
            ssl: {
                require: true,
                rejectUnauthorized: false,
            },
        },
    },
    // Elephant SQL
    test: {
        username: "meeobukf",
        password: "6OBZWJBgMz92HqeBMmRuoLBWj3N1Kal2",
        database: "meeobukf",
        host: "satao.db.elephantsql.com",
        port: DB_PORT,
        dialect: "postgres",
        ssl: true,
    },
    // Heroku Postgres
    production: {
        username: DB_USER,
        password: DB_PASSWORD,
        database: DB_NAME,
        host: DB_HOST,
        port: DB_PORT,
        dialect: "postgres",
        ssl: true,
        dialectOptions: {
            ssl: {
                require: true,
                rejectUnauthorized: false,
            },
        },
    },
};
