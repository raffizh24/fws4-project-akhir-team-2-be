const express = require("express");
const cors = require("cors");

const controllers = require("../app/controllers");
const handleGoogleLoginOrRegister = require("../app/controllers/api/v1/handleGoogleLoginOrRegister");
const productsController = require("../app/controllers/api/v1/productsController");
const userController = require("../app/controllers/api/v1/userController");
const transactionController = require("../app/controllers/api/v1/transactionController");
const uploadOnMemory = require("./uploadOnMemory");

const apiRouter = express.Router();
apiRouter.use(cors());
apiRouter.use(express.json());

// API Authentication & Authorization
apiRouter.post("/api/v1/register", userController.register);
apiRouter.post("/api/v1/login", userController.login);
apiRouter.post("/api/v1/auth/google", handleGoogleLoginOrRegister);
apiRouter.get("/api/v1/auth/me", userController.whoAmI);
apiRouter.delete("/api/v1/auth/delete/:email", userController.deleteUser);
// API User
apiRouter.put("/api/v1/auth/me", userController.authorization, uploadOnMemory.single("fotoUser"), userController.updateInfoUser);
// API Products
apiRouter.get("/api/v1/products", productsController.listAllProducts);
apiRouter.get("/api/v1/product/:id", productsController.getProductById);
apiRouter.get("/api/v1/product/filter/kategori", productsController.getProductByKategori);
apiRouter.get("/api/v1/product/filter/status", productsController.getProductsByStatus);
apiRouter.get("/api/v1/product/filter/nama", productsController.getProductByName);
apiRouter.post("/api/v1/product", userController.authorization, uploadOnMemory.array("fotoProduk", 4), productsController.createProduct);
apiRouter.put("/api/v1/product/:id", userController.authorization, uploadOnMemory.array("fotoProduk", 4), productsController.updateProduct);
apiRouter.delete("/api/v1/product", userController.authorization, productsController.deleteProduct);
// API Transactions
apiRouter.get("/api/v1/trBuyer", userController.authorization, transactionController.getByIdBuyer);
apiRouter.get("/api/v1/trSeller", userController.authorization, transactionController.getByIdSeller);
apiRouter.post("/api/v1/transaction", userController.authorization, transactionController.createTransaction);
apiRouter.put("/api/v1/transaction", userController.authorization, transactionController.updateTransaction);
apiRouter.put("/api/v1/transaction/status", userController.authorization, transactionController.updateTransactionStatus);
apiRouter.delete("/api/v1/transaction/:id", userController.authorization, transactionController.deleteTransaction);
// API Others
apiRouter.get("/", controllers.api.main.onSuccess);

apiRouter.get("/api/v1/errors", () => {
    throw new Error("Second Hand Error");
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
